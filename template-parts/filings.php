<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 28.10.17
 * Time: 20:22
 */

?>

<div class="tab-pane" id="block-4">
    <div class='officers'>
        <h3 class="title"><?= count($company->filings) ?> найдено</h3>
        <?php foreach ($company->filings as $filing) :
            $item = $filing->filing; ?>
            <div class="officer">
                <h2 class="title"><?= $item->title ?></h2>
                <?php if (property_exists($item, 'description') && $item->description) : ?>
                <h4 class='title'>Описание</h4>
                    <p class="title">
                        <?= $item->description; ?>
                    </p>
                <?php endif; ?>

                <ul class='description'>

                    <?php if ($item->date) : ?>
                        <li>
                            <h4 class="title">Дата</h4>
                            <p class="title"> <?= formattingDateOC($item->date) ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if ($item->filing_type_code) : ?>
                        <li>
                            <h4 class="title">Код</h4>
                            <p class="title"> <?= $item->filing_type_code ?></p>
                        </li>
                    <?php endif; ?>

                </ul>

            </div>
        <?php endforeach; ?>
    </div>
</div>
