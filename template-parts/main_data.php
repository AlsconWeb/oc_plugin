<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 28.10.17
 * Time: 20:21
 */

?>

<div class="tab-pane active" id="block">
	<?php if ( $company->registered_address ) : ?>
		<h4 class='title'>Юридический адрес</h4>
		<p class="title">
			<?= $company->registered_address->street_address ? $company->registered_address->street_address . ', ' : '' ?>
			<?= $company->registered_address->locality ? $company->registered_address->locality . ', ' : '' ?>
			<?= $company->registered_address->region ? $company->registered_address->region . ', ' : '' ?>
			<?= $company->registered_address->postal_code ? $company->registered_address->postal_code . ', ' : '' ?>
			<?= $company->registered_address->country ? $company->registered_address->country . ' ' : '' ?>
		</p>
	<?php endif; ?>

	<?php if ( $company->current_status ) : ?>
		<h4 class='title'>Статус компании</h4>
		<p class="company-title title">
			<?= $company->current_status ?>
		</p>
	<?php endif; ?>

	<?php if ( $company->company_type ) : ?>
		<h4 class='title'>Тип компании</h4>
		<p class="company-title title">
			<?= str_replace( "-", " ", $company->company_type ) ?>
		</p>
	<?php endif; ?>

	<?php if ( $company->jurisdiction_code ) : ?>
		<h4 class='title'>Код юрисдикции</h4>
		<p class="company-title title">
			<?= getValueByISOCode( $company->jurisdiction_code ); ?>
		</p>
	<?php endif; ?>

	<?php if ( $company->incorporation_date ) : ?>
		<h4 class='title'>Дата основания</h4>
		<p class="company-title title">
			<?= formattingDateOC( $company->incorporation_date ) ?>
		</p>
	<?php endif; ?>

	<?php if ( $company->dissolution_date ) : ?>
		<h4 class='title'>Дата закрытия</h4>
		<p class="company-title title">
			<?= formattingDateOC( $company->dissolution_date ) ?>
		</p>
	<?php endif; ?>

	<?php if ( $company->source->publisher ) : ?>
		<h4 class='title'>Источник</h4>
		<p class="company-title title">
			<?= $company->source->publisher ?>
		</p>
		<a href="<?= $company->source->url ?>" target="_blank" rel="nofollow">
			<p class="company-title title">
				<?= $company->source->url ?>
			</p>
		</a>
	<?php endif; ?>

	<?php if ( $company->industry_codes ) : ?>
		<h4 class="title">Вид деятельности (SIC)</h4>
		<?php foreach ( $company->industry_codes as $industry_code ): ?>
			<p class="title">
				<?= getValueBySIC( $industry_code->industry_code ) ?>
			</p>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if ( $company->branch_status ) : ?>
		<h4 class="title"><?= $company->branch_status == 'F' ? 'филиал' : 'местный офис' ?></h4>
	<?php endif; ?>

	<?php if ( $company->number_of_employees ) : ?>
		<h4 class='title'>Кол-во работников</h4>
		<p class="company-title title">
			<?= $company->number_of_employees; ?>
		</p>
	<?php endif; ?>

	<?php if ( $company->previous_names ) : ?>
		<h4 class="title">Предидущее имя компании </h4>
		<?php foreach ( $company->previous_names as $company_name ) : ?>
			<p class="title"><?= $company_name->company_name . ' (' . formattingDateOC( $company_name->start_date ) . ( $company_name->end_date ? ' - ' . formattingDateOC( $company_name->end_date ) : '' ) . ') ' ?></p>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if ( $company->alternative_names ) : ?>
		<h4 class="title">Альтернативное имя компании </h4>
		<?php foreach ( $company->alternative_names as $company_name ) : ?>
			<p class="title"><?= $company_name->company_name; ?></p>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if ( property_exists( $company, 'data' ) && ! empty( $company->data->most_recent ) ) :
		$i = 0; ?>
		<?php foreach ( $company->data->most_recent as $item ):
		?>
		<?php if ( ! empty( $item->datum->title ) && ! empty( $item->datum->description ) ): ?>
		<h4 class="title"><?= $item->datum->title; ?> </h4>
		<p class="title"><?= $item->datum->description; ?></p>
	<?php endif; ?>
	<?php endforeach; ?>
	<?php endif; ?>

	<?php if ( property_exists( $company, 'officers' ) && $company->officers ):
		$officers_inactive = []; ?>
		<?php foreach ( $company->officers as $officer ): ?>
		<?php if ( $officer->officer->inactive ) {
			array_push( $officers_inactive, $officer->officer );
		} ?>
	<?php endforeach; ?>
		<?php if ( $officers_inactive ): ?>
		<h4 class="title">Неактивные директора/офицеры (<?= count( $officers_inactive ); ?>)</h4>
		<?php foreach ( $officers_inactive as $officer ): ?>
			<p class="title"><?= $officer->name . ', ' . $officer->position; ?></p>
		<?php endforeach; ?>
	<?php endif; ?>
	<?php endif; ?>

	<?php if ( property_exists( $company, 'identifiers' ) && $company->identifiers ) : ?>
		<?php foreach ( $company->identifiers as $item ): ?>
			<h4 class="title"><?= $item->identifier->identifier_system_name; ?></h4>
			<p class="title"><?= $item->identifier->uid; ?></p>
		<?php endforeach; ?>
	<?php endif; ?>


</div>
