<?php

function theme_settings_page_oc()
{

    echo '<div class="wrap">';
    echo '<h1>Open Corporates</h1>';
    echo '<span>шорткод для использования: [oc_search]</span>';
    echo '<form method="post" action="options.php" enctype="multipart/form-data">';
    settings_fields("oc_section");
    do_settings_sections("theme-options-oc");
    submit_button();
    echo '</form>';
    echo '</div>';
}

function oc_display_api_key()
{
    echo '<input type="text" name="oc_search_api_key" id="oc_search_api_key" value="' . get_option('oc_search_api_key') . '" size="50"/>';
}

function oc_display_url()
{
    echo '<input type="text" name="oc_url" id="oc_url" value="' . get_option('oc_url') . '" size="50"/>';
}

function oc_display_theme_panel_fields()
{
    add_settings_section("oc_section", "", null, "theme-options-oc");
    add_settings_field("oc_search_api_key", "API key", "oc_display_api_key", "theme-options-oc", "oc_section");
    add_settings_field("oc_url", "Ссылка на страницу с плагином CompanyHouse", "oc_display_url", "theme-options-oc", "oc_section");

    register_setting("oc_section", "oc_search_api_key");
    register_setting("oc_section", "oc_url");
}

add_action("admin_init", "oc_display_theme_panel_fields");

function add_theme_menu_item_oc()
{
    add_menu_page("", "Open Corporates", "manage_options", "theme-panel2", "theme_settings_page_oc", null, 99);
}

add_action("admin_menu", "add_theme_menu_item_oc");

