<?php

/**
 * @param     $val
 * @param int $index
 *
 * @return mixed
 */

function searchOC( $val, $page = 1 ) {
	$search_param = str_replace( ' ', '+', trim( $val ) );
	$curl         = curl_init();
	curl_setopt( $curl, CURLOPT_URL, 'https://api.opencorporates.com/v0.4/companies/search?q='
	                                 . $search_param . '&per_page=20&api_token='
	                                 . get_option( 'oc_search_api_key' )
	                                 . '&format=json'
	                                 . '&page=' . $page );
	curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'GET' );
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $curl, CURLOPT_HEADER, false );
	curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, 0 );
	curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
	$response = curl_exec( $curl );
	if ( $errno = curl_errno( $curl ) ) {
		$error_message = curl_strerror( $errno );
		echo "cURL error ({$errno}):\n {$error_message}";
	}
	curl_close( $curl );

	return json_decode( $response );
}

/**
 * @param        $id
 * @param string $type
 * filing-history, officers, persons-with-significant-control
 *
 * @return mixed
 */
function searchOCDataById( $jurisdiction_code, $company_number ) {
	$curl = curl_init();
	curl_setopt( $curl, CURLOPT_URL, 'https://api.opencorporates.com/v0.4/companies/'
	                                 . $jurisdiction_code . '/'
	                                 . $company_number
	                                 . '?api_token=' . get_option( 'oc_search_api_key' )
	                                 . '&format=json' );
	curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'GET' );
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $curl, CURLOPT_HEADER, false );
	curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, 0 );
	curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
	$response = curl_exec( $curl );
	if ( $errno = curl_errno( $curl ) ) {
		$error_message = curl_strerror( $errno );
		echo "cURL error ({$errno}):\n {$error_message}";
	}
	curl_close( $curl );

	return json_decode( $response );
}

/**
 * @param $date
 *
 * @return string
 */
function formattingDateOC( $date ) {
	if ( $date ) {
		return implode( '.', array_reverse( explode( '-', $date ) ) );
	}
}

/**
 * @param $code_sic
 *
 * @return mixed
 */
function getValueBySIC( $code_sic ) {
	$handle = fopen( __DIR__ . "/SIC07.csv", "r" );
	$res    = '';
	while ( ( $data = fgetcsv( $handle, 2000, ',' ) ) !== false ) {
		if ( (int) $data[0] == (int) $code_sic->code ) {
			$res = $code_sic->code . ': ' . $data[1];

			return $res;
			fclose( $handle );
		}
	}
	if ( ! $res ) {
		$res = $code_sic->code
		       . ': ' . $code_sic->description
		       . ' (' . $code_sic->code_scheme_name . ')';
	}

	return $res;
}

function getValueByISOCode( $code ) {
	global $wpdb;
	$code = mb_strtoupper( esc_sql( $code ) );
	$res  = $wpdb->get_results( 'SELECT `name` FROM `iso_countries`
	                          WHERE `alpha2` = "' . $code . '" 
	                          OR `alpha3` = "' . $code . '"' );
	if ( $res ) {
		return $res ? ( $code . ': ' . $res[0]->name ) : $code;
	}

	return '';
}

/**
 * @param $company
 *
 * @return string
 */
function createUrl( $company ) {
	if ( $company->company->jurisdiction_code == 'gb' ) {
		return get_option( 'oc_url' ) . '?company_id=' . $company->company->company_number;
	}

	return '?j_code=' . $company->company->jurisdiction_code . '&company_number=' . $company->company->company_number;
}

/**
 * @param $max_pages
 *
 * @return int
 */
function createCounter( $max_pages ) {
	$res = (int) round( $max_pages / 100 );

	return $res > 0 ? $res : 1;
}
