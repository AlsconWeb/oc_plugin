<div id="search-app" class="col-xs-12">
    <form class="form-horizontal" method="get" action="#">
        <input type="search" name="val" class="form-control" placeholder="Введите название компании"
               value="<?= isset($_GET['val']) ? $_GET['val'] : '' ?>">
        <input type="submit" value="Поиск">
    </form>
</div>

<?php

if (isset($_GET['val']) && strlen($_GET['val']) >= 2) :

    $data = searchOC(esc_sql($_GET['val']), (isset($_GET['oc-page']) ? (int)$_GET['oc-page'] : 1));
    include(plugin_dir_path(__FILE__) . 'template-parts/search_results.php');

elseif (isset($_GET['j_code']) && isset($_GET['company_number'])) :
    $company_data = searchOCDataById(esc_sql($_GET['j_code']), esc_sql($_GET['company_number']));
    if ($company_data->results) :
        $company = $company_data->results->company;
        ?>
        <h2 class="company-title title"><?= $company->name; ?></h2>
        <h4 class="title"> Номер компании: <?= $company->company_number ?></h4>
        <ul class="nav nav-tabs">
            <li class="active"><a href="#block" class="search-result-item" data-toggle="tab">Обзор</a></li>
            <?php if ($company->trademark_registrations): ?>
                <li><a href="#block-2" class="search-history-item" data-toggle="tab">Торговые марки</a></li>
            <?php endif;
            if ($company->filings): ?>
                <li><a href="#block-4" class="search-history-item" data-toggle="tab">Документы</a></li>
            <?php endif; ?>
            <?php if ($company->officers): ?>
                <li><a href="#block-3" class="search-people-item" data-toggle="tab">Люди</a></li>
            <?php endif; ?>
        </ul>
        <div class="tab-content" id="ch-plugin-main-block">

            <!--            main (start) -->
            <?php include(plugin_dir_path(__FILE__) . 'template-parts/main_data.php'); ?>
            <!--            main (end) -->
            <!--            trademarks (start) -->
            <!--            example ?j_code=gb&company_number=00102498 -->
            <?php if ($company->trademark_registrations):
                include(plugin_dir_path(__FILE__) . 'template-parts/trademarks.php');
            endif; ?>
            <!--            history (end) -->
            <!--            officers (start) -->
            <?php include(plugin_dir_path(__FILE__) . 'template-parts/officers.php'); ?>
            <!--            officers (end) -->
            <!--            filings (start) -->
            <?php if ($company->filings):
                include(plugin_dir_path(__FILE__) . 'template-parts/filings.php');
            endif; ?>
            <!--            filings (end) -->
            <div id="robinzon"><a href='#' OnClick="history.back();" class='button'>Вернуться обратно</a></div>
        </div>

    <?php endif;

endif; ?>
